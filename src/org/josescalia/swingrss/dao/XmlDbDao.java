package org.josescalia.swingrss.dao;


import org.josescalia.swingrss.model.Rss;
import org.josescalia.swingrss.model.RssItem;
import org.josescalia.swingrss.util.XmlUtility;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Josescalia
 * Date: 11/28/12
 * Time: 7:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class XmlDbDao {
  
    private File dbFile;
    private List<Rss> listRss;

    public XmlDbDao() {
        this.dbFile = new File("assets\\db.xml");
    }

    public XmlDbDao(File dbFile) {
        this.dbFile = dbFile;
    }

    private boolean writeDb(List<Rss> listRss) {
        XmlUtility utility = new XmlUtility();
        FileOutputStream fop = null;
        Document doc = utility.createRssXMLTemp(listRss);
        String output = utility.serializeDom(doc);
        //time to write to DB
        try {
            fop = new FileOutputStream(dbFile);
            // if file doesnt exists, then create it
            if (!dbFile.exists()) {
                dbFile.createNewFile();
            }

            // get the content in bytes
            byte[] contentInBytes = output.getBytes();

            fop.write(contentInBytes);
            fop.flush();
            fop.close();

            System.out.println("Done");
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return false;
        } catch (IOException ioe) {
            ioe.printStackTrace();
            return false;
        }
    }

    public Long getNewId() {
        int lastId = getRssList().size();
        return (long) lastId + 1;
    }

    public boolean deleteRss(Long id) {
        boolean rBool = false;
        listRss = getRssList();
        for (Rss s : listRss) {
            if (id.equals(s.getId())) {
                System.out.println("yg mau di delete : " + s);
                listRss.remove(s);
                rBool = true;
                break;
            }
        }


        List<Rss> newRssList = new ArrayList<Rss>();
        //since using id, after deletion we must rearrange id
        for (int i = 0; i < listRss.toArray().length; i++) {
            Rss s = listRss.get(i);
            //resetting id
            s.setId((long) i + 1);
            //then add to new arrayList
            newRssList.add(s);
        }

        //write to file using new list
        return writeDb(newRssList);
    }

    public List<Rss> getRssList() {
        listRss = new ArrayList<Rss>();
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder;
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(dbFile);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("rss");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    Rss rss = new Rss();
                    rss.setId(Long.valueOf(getTagValue("id", eElement)));
                    rss.setTitle(getTagValue("title", eElement));
                    rss.setLink(getTagValue("link", eElement));
                    listRss.add(rss);
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return listRss;
    }

    private static String getTagValue(String sTag, Element eElement) {
        try {
            NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();
            Node nValue = (Node) nlList.item(0);
            if (nValue.getNodeValue() != null || !nValue.getNodeValue().isEmpty()) {
                return nValue.getNodeValue();
            } else {
                return "";
            }
        } catch (Exception e) {
            return "";
        }
    }

    public File getDbFile() {
        return dbFile;
    }

    public void setDbFile(File dbFile) {
        this.dbFile = dbFile;
    }

    public static void main(String[] args) {
        XmlDbDao dao = new XmlDbDao();
        List<Rss> rssList = new ArrayList<Rss>();
        rssList = dao.getRssList();
        Rss rss = rssList.get(3);
        //rss.setTitle("Torrent Movies");
        //rss.setLink("http://torrentz.eu/feed?q=.movies");
        dao.deleteRss((long) 4);

        //dao.writeDb(rssList);
    }


    public boolean add(Rss rss) {
        boolean bRes = false;
        List<Rss> listRss = getRssList();
        listRss.add(rss);
        return writeDb(listRss);
    }

    public boolean edit(Rss rss){
        List<Rss> newListRss = new ArrayList<Rss>();
        List<Rss> listRss = getRssList();
        for (Rss s: listRss){
            if(s.getId().equals(rss.getId())){
                s.setId(rss.getId());
                s.setTitle(rss.getTitle());
                s.setLink(rss.getLink());
                newListRss.add(s);
            }else{
                newListRss.add(s);
            }
        }
        return writeDb(newListRss);
    }
}
