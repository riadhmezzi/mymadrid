package org.josescalia.swingrss.services;

import org.josescalia.swingrss.dao.XmlDbDao;
import org.josescalia.swingrss.model.Rss;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Josescalia
 * Date: 11/28/12
 * Time: 7:11 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Service layer class to handle the data
 */
public class RssService {

    XmlDbDao dao;

    public RssService() {
        init();
    }


    private void init() {
        dao = new XmlDbDao();
    }

    public List<Rss> getRssList() {
        return dao.getRssList();
    }

    public boolean deleteRss(Long id){
        return dao.deleteRss(id);
    }

    public Rss getNewRss() {
        Rss result = new Rss();
        result.setId(dao.getNewId());
        return result;
    }

    public boolean saveOrUpdate(Rss rss){
        if(dao.getRssList().size() < rss.getId()) { //new Rss
            return dao.add(rss);
        }else{
            return dao.edit(rss);
        }
    }

    public static void main(String[] args) {
        RssService rssService = new RssService();
        //rssService.init();
        for (Rss rss : rssService.getRssList()) {
            System.out.println("---------------------------");
            System.out.println("title :" + rss.getTitle());
            System.out.println("link  :" + rss.getLink());
            System.out.println("===========================");
        }
    }


}
