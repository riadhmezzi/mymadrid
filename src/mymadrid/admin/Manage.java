/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mymadrid.admin;

import java.io.*;
import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Vector;
import javax.imageio.ImageIO;
import mymadrid.dao.PlayerDao;
import mymadrid.entite.Match;
import mymadrid.entite.Player;

/**
 *
 * @author Riadh
 */
public class Manage {
    
    
    public static void main(String[] args) throws SQLException, IOException {
    Vector<Player> listPlayers ; 
    PlayerDao pdao = new PlayerDao();
    Player cassilas = new Player("Iker", "Casillas Fernandez", new Date(81, 05, 20)  , "Mostoles, Spain", "Goalkeeper", "182.2 cm", "85.5 kg", "Spanish", "I am a winner whose work is based on humiliy", 1 ,"./images/casillas.jpg");
    
    
    Player ALONSO = new Player("XABI", "Alonso Olano", new Date(81, 11, 25)  , "Tolosa, Spain", "Midfielder", "182.7 cm", "79 kg", "Spanish", "I am simply a team player", 14 ,"./images/alonso.jpg");
    Player OZIL = new Player("Mesut", "OZIL", new Date(88, 10, 15)  , "Gelsenkirchen, Germany", "Midfielder", "181 cm", "70 kg", "German", "I will do my best to win titles with Real Madrid", 10 ,"./images/ozil.jpg");
    Player PEPE = new Player("PEPE", "Kléper Laverán", new Date(86, 02, 26)  , "Maceió, Brazil", "Defender", "187.1 cm", "81 kg", "Portuguese/Brazilian", "I have character and I am devoted to this club and to my team-mates", 3 ,"./images/pepe.jpg");
    Player ronaldo = new Player("CRISTIANO RONALDO", "dos Santos Aveiro", new Date(85, 02, 05)  , "Madeira, Portugal", "Forward", "186.5 cm", "84.5 kg", "Portuguese", "I am an ambitious player with only one objective: victory fo my team", 7 ,"./images/ronaldo.jpg");
    
    Player Kaka = new Player("KAKA", "Ricardo Izecson Dos Santos", new Date(82, 04, 22)  , "Brasilia, Brazil", "Midfielder", "185.6 cm", "82 kg", "Brazilian", "I dream about a great play every day of my life", 8 ,"./images/kaka.jpg");
    
    Player Benzima = new Player("BENZEMA", "Karim", new Date(87, 12, 19)  , "Lyon, France", "Forward", "184.1 cm", "83.5 kg", "French", "I am a striker who loves good football", 9 ,"./images/benzima.jpg");
    
     pdao.InsertPlayer(Kaka);
    pdao.InsertPlayer(Benzima);
    
//    pdao.InsertPlayer(ALONSO);
//    pdao.InsertPlayer(OZIL);
//    pdao.InsertPlayer(PEPE);
//    pdao.InsertPlayer(ronaldo);
    
   // pdao.InsertPlayer(cassilas); 
        
        
       // listPlayers = pdao.SelectPlaeyrs() ; 
        
       // System.out.println(listPlayers.);
        
  //  extractPlayersImages(); 
    
        
    }

    public static void extractPlayersImages() throws SQLException, FileNotFoundException, IOException {
       PlayerDao pdao = new PlayerDao(); 
       
        for (Player p : pdao.SelectPlaeyrs()) {
                        System.out.println(p.getImage()); 
                        InputStream is = p.getImage() ; 
                        //File file = new File
                        FileOutputStream fos = new FileOutputStream(".\\imageFromDB\\"+p.getNom() +".jpg"); 
                        int b = 0;  
                        while ((b = is.read()) != -1)  
                        {  
                            fos.write(b); // fos is the instance of FileOutputStream  
                        }  
            
                    }
    }
    
}
