package mymadrid.technique;
import java.sql.*;
/**
 *
 * @author Riadh
 */
public class ConnexionBD {

    static Connection connect;
    static String Driver = "com.mysql.jdbc.Driver";
    static String URL = "jdbc:mysql://localhost/mymadrid";
    static String User ="root";
    static String Pass = "";

    private ConnexionBD() throws ClassNotFoundException, SQLException{
        Class.forName(Driver);
        connect = DriverManager.getConnection(URL, User, Pass);
    }

    public static Connection getInstance() throws ClassNotFoundException, SQLException{

        if(connect == null){

            new ConnexionBD();
            System.out.println("Instanciation de la connexion SQL");
        }
        
        return connect;
     
    }
}
