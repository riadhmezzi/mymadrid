package mymadrid.entite;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Date;
import java.util.Calendar;


public class Player  {

    
        private int id;

    public int getId() {
        return id;
    }
	private String Nom;
	private String prenom;
	private Date date_naiss; 
        private String ville_naiss;
        private String position ; 
        private String longeur ;
        private String poids; 
        private String nationalite ; 
        private String citation ; 
        private int number ; 
        private InputStream  image;
        private String  imagePath;

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getLongeur() {
        return longeur;
    }

    public void setLongeur(String longeur) {
        this.longeur = longeur;
    }
    
    
        public Player(){
         
        }

    public Player(String Nom,String prenom, Date date_naiss, String ville_naiss, String position,String longeur, String poids, String nationalite, String citation, int number, String  imagePath) {
        this.Nom = Nom;
        this.prenom = prenom;
        this.date_naiss = date_naiss;
        this.ville_naiss = ville_naiss;
        this.position = position;
        this.longeur = longeur ; 
        this.poids = poids;
        this.nationalite = nationalite;
        this.citation = citation;
        this.number = number;
        this.imagePath = imagePath;
    }

    public Player(String Nom, String prenom, Date date_naiss, String ville_naiss, String position,String longeur ,String poids, String nationalite, String citation, int number) {
        this.Nom = Nom;
        this.prenom = prenom;
        this.date_naiss = date_naiss;
        this.ville_naiss = ville_naiss;
        this.position = position;
        this.longeur = longeur ; 
        this.poids = poids;
        this.nationalite = nationalite;
        this.citation = citation;
        this.number = number;
    }
        
    public String getNom() {
        return Nom;
    }

    public void setNom(String Nom) {
        this.Nom = Nom;
    }

    public String getCitation() {
        return citation;
    }

    public void setCitation(String citation) {
        this.citation = citation;
    }

    public Date getDate_naiss() {
        return date_naiss;
    }

    public void setDate_naiss(Date date_naiss) {
        this.date_naiss = date_naiss;
    }

    public InputStream getImage() {
        return image;
    }

    public void setImage(InputStream image) {
        this.image = image;
    }

    public String getNationalite() {
        return nationalite;
    }

    public void setNationalite(String nationalite) {
        this.nationalite = nationalite;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getPoids() {
        return poids;
    }

    public void setPoids(String poids) {
        this.poids = poids;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getVille_naiss() {
        return ville_naiss;
    }

    public void setVille_naiss(String ville_naiss) {
        this.ville_naiss = ville_naiss;
    }

    
    
   

    public void setId(int id) {
        this.id = id;
    }



        
    
        
	
	
}
