/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mymadrid.entite;

/**
 *
 * @author riadh
 */
public class UserApp {

    private int userId;
    private String userName;
    private String fbEmail;


    public UserApp(){}
    
    public UserApp (String userName , String fbEmail){
        this.userName= userName;
        this.fbEmail = fbEmail;
    }

    public String getFbEmail() {
        return fbEmail;
    }

    public void setFbEmail(String fbEmail) {
        this.fbEmail = fbEmail;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }




    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserApp other = (UserApp) obj;
        if ((this.fbEmail == null) ? (other.fbEmail != null) : !this.fbEmail.equals(other.fbEmail)) {
            return false;
        }
        return true;
    }



    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + this.userId;
        hash = 83 * hash + (this.userName != null ? this.userName.hashCode() : 0);
        return hash;
    }




}
