package mymadrid.entite;

import java.sql.Date;
import java.util.Calendar;

public class Match implements Comparable<Match> {

    public void setDate(Date date) {
        this.date = date;
    }

    public void setGuestScore(int guestScore) {
        this.guestScore = guestScore;
    }

    public void setGuestTeam(String guestTeam) {
        this.guestTeam.setName(guestTeam);
    }

    public void setHomeScore(int homeScore) {
        this.homeScore = homeScore;
    }

    public void setHomeTeam(String homeTeam) {
        this.homeTeam.setName(homeTeam) ;
    }

    public void setId(int id) {
        this.id = id;
    }
	private int id;
	private Date date;
	private Team homeTeam;
	private Team guestTeam;
	private int homeScore = -1;
	private int guestScore = -1;

        
        public Match(){
            homeTeam  = new Team();
            guestTeam = new Team() ; 
        }
        
	public Match(int matchid,Date date, Team homeTeam, Team guestTeam, int homeScore, int guestScore) {
                id=matchid;	
                  this.date = date;
		this.homeTeam = homeTeam;
		this.guestTeam = guestTeam;
		this.homeScore = homeScore;
		this.guestScore = guestScore;
		
	}

	public Match(int matchid,Date date, Team homeTeam, Team guestTeam) {
		this( matchid,date, homeTeam, guestTeam, -1, -1);
	}

	public int compareTo(Match match) {
		int result = getDate().compareTo(match.getDate());
		if (result != 0)
			return result;
		return getId()-match.getId();
	}

	@Override
	public String toString() {
		return getId() + "" ;
	}

	public int getId() {
		return id;
	}

	public Date getDate() {
		return date;
	}

	public Team getHomeTeam() {
		return homeTeam;
	}

	public Team getGuestTeam() {
		return guestTeam;
	}

	public int getHomeScore() {
		return homeScore;
	}

	public int getGuestScore() {
		return guestScore;
	}
}
