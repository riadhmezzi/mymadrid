/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mymadrid.mail;

import java.io.UnsupportedEncodingException;
import javax.mail.PasswordAuthentication;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author riadh
 */
public class MailConstruction {
      
    URL url;
    Properties prop;
    String mailPart;
    Session session;
    Message message;
    
    //Ouvrir une session
    public void getMailPropreties()
    
    {
        this.prop=new Properties();
        this.prop.put("mail.smtp.auth", "true");
        this.prop.put("mail.smtp.starttls.enable", "true");
        this.prop.put("mail.smtp.host", "smtp.gmail.com");
        this.prop.put("mail.smtp.port", "587");
       
     
        Session.getInstance(prop);
       }
    
    public void getMailMessage(String pieceJointe,final Mail mail) throws UnsupportedEncodingException
    {
        try {
            session=Session.getInstance(prop, new javax.mail.Authenticator() {
            
            protected  PasswordAuthentication getPasswordAuthentication()
            {
                return new PasswordAuthentication(mail.getAddressSender(), mail.getPassword());
            }
            
            });
            
              
            
                
            
            message=new MimeMessage(session);
            InternetAddress recepient=new InternetAddress(mail.getAddressRecipient(),mail.getAddressRecipient());
            message.setRecipient(Message.RecipientType.TO, recepient);
            message.setSubject(mail.getSubjectMail());
           
            
            //Le texte
            MimeBodyPart mbp1=new MimeBodyPart();
            mbp1.setText(mail.getObjectMail());
           // create the message part
    MimeBodyPart messageBodyPart =
      new MimeBodyPart();

    //fill message
    messageBodyPart.setText("");

    Multipart multipart = new MimeMultipart();
    multipart.addBodyPart(messageBodyPart);

//    // Part two is attachment
//    messageBodyPart = new MimeBodyPart();
//    DataSource source =
//      new FileDataSource("c:/a.txt");
//    messageBodyPart.setDataHandler(
//      new DataHandler(source));
//    messageBodyPart.setFileName("a.txt");

//
//            //La piece Jointe
//            MimeBodyPart mbp2=new MimeBodyPart();
//            String file=pieceJointe;
//            mbp2.setText(file);
//            //regrouper les deux dans le même message
            
            MimeMultipart mp=new MimeMultipart();
            mp.addBodyPart(mbp1);
        //    mp.addBodyPart(mbp2);
            mp.addBodyPart(messageBodyPart);
            message.setContent(mp);
        } 
        
        
      
        catch (MessagingException ex) {
            Logger.getLogger(MailConstruction.class.getName()).log(Level.SEVERE, null, ex);
        }       
        
        
    }
      public void sendMessage(){
        try {
            Transport.send(message);
        } catch (MessagingException ex) {
            Logger.getLogger(MailConstruction.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    
    
}
