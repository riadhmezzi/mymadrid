/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mymadrid.mail;

import java.io.File;



/**
 *
 * @author riadh
 */
public class Mail {
    
    private String addressSender;
    private String addressRecipient;
    private String password;
    
    private String objectMail;
    private String subjectMail;
    private File piece;

    public File getPiece() {
        return piece;
    }

    public void setPiece(File piece) {
        this.piece = piece;
    }

    public String getAddressRecipient() {
        return addressRecipient;
    }

    public void setAddressRecipient(String addressRecipient) {
        this.addressRecipient = addressRecipient;
    }

    public String getAddressSender() {
        return addressSender;
    }

    public void setAddressSender(String addressSender) {
        this.addressSender = addressSender;
    }

    public String getObjectMail() {
        return objectMail;
    }

    public void setObjectMail(String objectMail) {
        this.objectMail = objectMail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

   

    

    public String getSubjectMail() {
        return subjectMail;
    }

    public void setSubjectMail(String subjectMail) {
        this.subjectMail = subjectMail;
    }
    
    
    
}
