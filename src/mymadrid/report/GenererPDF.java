package mymadrid.report;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 * @author Riadh
 */
public class GenererPDF {

    public static void create() {
        // - Paramètres de connexion à la base de données
        Connection connection;
        try {
            // - Connexion à la base
            connection = MySQLConnexion.getInstance();
            // - Chargement et compilation du rapport
            JasperDesign jasperDesign = JRXmlLoader.load(".\\allmatches.jrxml");
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            // - Paramètres à envoyer au rapport
            Map parameters = new HashMap();
            parameters.put("Titre", "Titre");
            // - Execution du rapport
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, connection);
            // - Création du rapport au format PDF
            JasperExportManager.exportReportToPdfFile(jasperPrint,".\\allmatches.pdf");
            System.out.println("Pdf Generation success");
            File file = new File(".\\allmatches.pdf");
    if (file.toString().endsWith(".pdf"))
        Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + file);
    else {
        Desktop desktop = Desktop.getDesktop();
        desktop.open(file);
}
            
          
           
            
            
        } catch (IOException ex) {
            Logger.getLogger(GenererPDF.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JRException e) {
            System.out.println("erreur de compilation" + e.getMessage());
        }
    }
}
