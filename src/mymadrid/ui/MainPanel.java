package mymadrid.ui;

import static aurelienribon.slidinglayout.SLSide.BOTTOM;
import static aurelienribon.slidinglayout.SLSide.LEFT;
import static aurelienribon.slidinglayout.SLSide.RIGHT;
import static aurelienribon.slidinglayout.SLSide.TOP;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.JLabel;


import mymadrid.utils.Res;
import mymadrid.utils.SwingUtils;



import aurelienribon.slidinglayout.SLAnimator;
import aurelienribon.slidinglayout.SLConfig;
import aurelienribon.slidinglayout.SLKeyframe;
import aurelienribon.slidinglayout.SLPanel;
import aurelienribon.ui.components.Button;
import aurelienribon.ui.components.PaintedPanel;
import aurelienribon.ui.css.Style;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import mymadrid.fb.fbCnx;
import mymadrid.gallery.Gallery;
import mymadrid.mail.MailFrame;
import mymadrid.ui.panels.*;
import org.josescalia.swingrss.gui.form.RssReaderForm;

/**
 * @author Riadh
 */
public class MainPanel extends PaintedPanel {

    public Component getContext() {
       return this; 
    }

	
    //cnxFb
    private fbCnx fbcnx ; 
    

        MailFrame mail=new MailFrame(this);
        private Charts charts=new Charts();
        private RssReaderForm rss=new RssReaderForm(this);
        private AllMatches allMatches = new AllMatches(this); 
        private PlayersInfo playersPanel ; 
        private Classement classement=new Classement(this);
        private Welcom welcom=new Welcom(this);

     
        
        private Gallery gallery = new Gallery(); 
        
	


        private SLConfig galleryCfg;

        
        private SLConfig allMatchesCfg;



	// Panels


	// Start panel components
	private final JLabel startLogoLabel = new JLabel(Res.getImage("gfx/logo.png"));
	private final JLabel startQuestionLabel = new JLabel("<html><center>Welcome to myMadrid the funKoora application for Madridista... </center></Html>");
	private final Button startBtn = new Button() {{setText("Login with your Facebook"); }};
    
	

	// Misc components



	// SlidingLayout
	private final SLPanel rootPanel = new SLPanel();
	private final float transitionDuration = 0.5f;
	private final int gap = 10;
	private SLConfig initCfg;
	private SLConfig startCfg;
        private SLConfig chartsCfg;
         private SLConfig mailCfg;

	public MainPanel() {
        try {
            playersPanel = new PlayersInfo(this);
        } catch (SQLException ex) {
            Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
            
             fbcnx = new fbCnx(this); 
		SwingUtils.importFont(Res.getStream("fonts/SquareFont.ttf"));
		setLayout(new BorderLayout());
		add(rootPanel, BorderLayout.CENTER);

		

		initUI();
		
		initStyle();
		initConfigurations();
		rootPanel.initialize(initCfg);

		SLAnimator.start();
		rootPanel.setTweenManager(SLAnimator.createTweenManager());
		

                
                
                
                

		SwingUtils.addWindowListener(this, new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				
			}

			@Override
			public void windowClosing(WindowEvent e) {
			
			}
		});
	
       
        
        }

	private void initConfigurations() {
		// TODO Auto-generated method stub
		initCfg = new SLConfig(rootPanel)
		.gap(gap, gap)
		.row(1f).row(30).col(1f)
		.beginGrid(0, 0)
			.row(startLogoLabel.getPreferredSize().height+ 20)
			.row(100)
			.col(1f)
			.place(0, 0, startLogoLabel)
			.beginGrid(1, 0)
				.row(1f).row(50).row(80).row(1f)
				.col(1f).col(400).col(1f)
				.place(1, 1, startQuestionLabel)
				.beginGrid(2, 1)
					.row(1f)
					.col(1f)
					.place(0, 0, startBtn)
					
				.endGrid()
			.endGrid()
		.endGrid()
		;
                
		startCfg = new SLConfig(rootPanel)
		.gap(gap, gap)
		.row(1f).col(310).col(1f)
		.beginGrid(0, 0)
                    .row(1f)
                    .col(305)
                    .place(0, 0,classement)
		.endGrid()
                        
                .beginGrid(0, 1)			
                   .row(300)
                   .row(300)
                   .col(740)
                    .beginGrid(0,0)
                     .row(300)
                     .col(270)
                     .col(460)
                     .place(0, 0,welcom)   
                     .place(0, 1,rss)
                    .endGrid()
                    .beginGrid(1,0)
                     .row(230)
                     .col(1f)
                     .place(0, 0, playersPanel)
                     .endGrid() 
                .endGrid();
                        
                
                
                
                 //charts de Statistiques
                 chartsCfg = new SLConfig(rootPanel)
		.gap(gap, gap)
		.row(1f).col(300).col(1f)
		.beginGrid(0, 0)

				.row(1f)

				.col(305)
				.place(0, 0,classement)



		.endGrid()
                .beginGrid(0, 1)

				.row(1f)

				.col(1f)
				.place(0, 0,charts)



		.endGrid();
                 
                 //Gallery
                 galleryCfg = new SLConfig(rootPanel)
		.gap(gap, gap)
		.row(1f).col(300).col(1f)
		.beginGrid(0, 0)

				.row(1f)

				.col(305)
				.place(0, 0,classement)



		.endGrid()
                .beginGrid(0, 1)

				.row(1f)

				.col(1f)
				.place(0, 0,gallery)



		.endGrid();
                
                
                //affichage allMatches
                allMatchesCfg = new SLConfig(rootPanel)
		.gap(gap, gap)
		.row(1f).col(310).col(1f)
		.beginGrid(0, 0)
                    .row(1f)
                    .col(305)
                    .place(0, 0,classement)
		.endGrid()
                        
                .beginGrid(0, 1)			
                   .row(300)
                   .row(300)
                   .col(740)
                    .beginGrid(0,0)
                     .row(300)
                     .col(270)
                     .col(460)
                     .place(0, 0,welcom)   
                     .place(0, 1,allMatches)
                    .endGrid()
                    .beginGrid(1,0)
                     .row(230)
                     .col(1f)
                     .place(0, 0, playersPanel)
                     .endGrid() 
                .endGrid();
                
                // Send Mail
                   
               
                mailCfg = new SLConfig(rootPanel)
		.gap(gap, gap)
		.row(1f).col(310).col(1f)
		.beginGrid(0, 0)
                    .row(1f)
                    .col(305)
                    .place(0, 0,classement)
		.endGrid()
                        
                .beginGrid(0, 1)			
                   .row(300)
                   .row(300)
                   .col(740)
                    .beginGrid(0,0)
                     .row(300)
                     .col(270)
                     .col(460)
                     .place(0, 0,welcom)   
                     .place(0, 1,mail)
                    .endGrid()
                    .beginGrid(1,0)
                     .row(230)
                     .col(1f)
                     .place(0, 0, playersPanel)
                     .endGrid() 
                .endGrid();
                

                    

	}

	private void initStyle() {
		// TODO Auto-generated method stub
		Style.registerCssClasses(this, ".rootPanel");
		Style.registerCssClasses(startBtn, ".startButton");
	
		Style.registerCssClasses(startQuestionLabel, ".startQuestionLabel");
                //titre
		Style.registerCssClasses(classement, ".groupPanel", "#classement");
                //titre du flux rss et all matches
                Style.registerCssClasses(rss, ".groupPanel", "#rss");
                Style.registerCssClasses(allMatches, ".groupPanel", "#allMatches");
                Style.registerCssClasses(welcom, ".groupPanel", "#welcom");
                Style.registerCssClasses(playersPanel, ".groupPanel", "#players");
                Style.registerCssClasses(charts, ".groupPanel", "#statistics");
                Style.registerCssClasses(mail, ".groupPanel", "#mail");
                
              
                
                Component[] targets = new Component[] {this,mail,welcom,mail.jButton1,charts,playersPanel,allMatches,rss,classement,rss.jButton2,allMatches.jButton1,startBtn,startQuestionLabel};
		Style style = new Style(Res.getUrl("css/style.css"));
                
		for (Component target : targets) Style.apply(target, style);
                
             
                
                
	}


	
	private void initUI() {
		startBtn.addActionListener(new ActionListener() {
			@Override public void actionPerformed(ActionEvent e) {
			//login fb before start
                             
                            fbcnx.loginFb(); 
                      
				
			}
		});

		

	
	
	}
	public void showMainView() {
		
if(rootPanel!=null && startCfg!=null)
{
    
                
    
		rootPanel.createTransition()
			.push(new SLKeyframe(startCfg, transitionDuration)
				.setEndSideForOldCmps(BOTTOM)
				.setStartSideForNewCmps(TOP)).play()
			;
	
}
	}
        public void showStatistics() {
		rootPanel.createTransition()
			.push(new SLKeyframe(chartsCfg, transitionDuration)
				.setEndSideForOldCmps(TOP)
				.setStartSideForNewCmps(BOTTOM))
			.play();
	}
        
        
           public void showGallery() {
		rootPanel.createTransition()
			.push(new SLKeyframe(galleryCfg, transitionDuration)
				.setEndSideForOldCmps(BOTTOM)
				.setStartSideForNewCmps(TOP))
			.play();
	}
        
        
	
        public void showMainViewFistTime() {
		
if(rootPanel!=null && startCfg!=null)
{
    
                
    
		rootPanel.createTransition()
			.push(new SLKeyframe(startCfg, transitionDuration)
				.setStartSideForNewCmps(RIGHT)
				.setEndSideForOldCmps(LEFT)).play()
			;
	
}
	}




	public void showInitView() {
	

		rootPanel.createTransition()
			.push(new SLKeyframe(initCfg, transitionDuration)
				.setStartSideForNewCmps(LEFT)
				.setEndSideForOldCmps(RIGHT)
				)
			.play();
	}


        
        public void showAllMatchesPanel() {
		rootPanel.createTransition()
			.push(new SLKeyframe(allMatchesCfg, transitionDuration)
				.setEndSideForOldCmps(TOP)
				.setStartSideForNewCmps(BOTTOM))
			.play();
	}
         public void showmail() {
		rootPanel.createTransition()
			.push(new SLKeyframe(mailCfg, transitionDuration)
				.setEndSideForOldCmps(TOP)
				.setStartSideForNewCmps(BOTTOM))
			.play();
	}
        
};

