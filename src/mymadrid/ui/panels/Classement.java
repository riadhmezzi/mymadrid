/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mymadrid.ui.panels;

import aurelienribon.ui.css.Style;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.dnd.DropTarget;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.GroupLayout;
import mymadrid.ui.MainPanel;

/**
 *
 * @author Riadh
 */
public class Classement extends javax.swing.JPanel {
    private static MainPanel mainPanel ;
    private boolean clicToShowMatches = true;
    private boolean clicToShowGallery = true;
    private boolean clicToContactUs = true;
    private boolean clicToShowStatistics = true;
    
    private static int currentScreen = 1;  //1 main 2 allmatches 3 gallery 4 statstics  5 contact us
    /**
     * Creates new form Classement
     */
    public Classement(final MainPanel mainPanel) {
        this.mainPanel = mainPanel ; 
        initComponents();
        initLabesActions(); 
       
         
		
                

		
		Style.registerCssClasses(showMatches, ".linkLabel");
                Style.registerCssClasses(showGalerie, ".linkLabel");
                Style.registerCssClasses(showStatistics, ".linkLabel");
                Style.registerCssClasses(contactUs, ".linkLabel");
                 Style.registerCssClasses(jTable1, "#table");
        
    }
    

    //action des labes
      private void initLabesActions() {
        //show matches
  
          
          
        showMatches.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		showMatches.addMouseListener(new MouseAdapter() {
			
                    @Override
			public void mousePressed(MouseEvent e) {
                        
				if (currentScreen== 1 && clicToShowMatches) {
                                    
                                     
				 showMatches.setText("< Hide Matches");
                                   // System.out.println(showMatches.getText());
                                 currentScreen = 2 ; 
                                                   
                              clicToShowMatches = false ; 
                                
                                  mainPanel.showAllMatchesPanel();  
                                    
					
				}else if(currentScreen== 2 && !clicToShowMatches)  {
                                             
                                     clicToShowMatches = true ; 
					
                          
					currentScreen = 1 ; 
					showMatches.setText("Show Matches >");
                                        mainPanel.showMainView();
				}
			}
		});
          //show Gallery
                 
        showGalerie.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		showGalerie.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (currentScreen== 1 && clicToShowGallery) {
				 currentScreen = 3 ; 
					showGalerie.setText("< Hide Gallery");
                                    
                                    mainPanel.showGallery();   
                                        clicToShowGallery = false ; 
                                        
                                        
				}else if(currentScreen== 3 && !clicToShowGallery )   {
                                    	currentScreen = 1 ; 
					showGalerie.setText("Show Gallery >");
                                        currentScreen = 1 ; 
                                         clicToShowGallery = true ; 
                                    mainPanel.showMainView();
                     
				
				}
			}
		});
                //contact us
           contactUs.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		contactUs.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (currentScreen ==1 && clicToContactUs  ) {
				mainPanel.showmail()	;
                                

                                currentScreen = 4 ; 
                                    clicToContactUs = false;
					contactUs.setText("< Hide Contact Us");
				} else if(currentScreen== 4 && !clicToContactUs )  {
                                    mainPanel.showMainView();
                           currentScreen = 1 ; 
					clicToContactUs = true;
					contactUs.setText("Contact Us >");
				}
			}
		});       
                
                //showStats
                showStatistics.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		showStatistics.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (currentScreen ==1 && clicToShowStatistics ) {
				mainPanel.showStatistics()	;
                                
                                    currentScreen = 5 ; 
                                
                                    clicToShowStatistics = false;
					showStatistics.setText("< Hide Statistics");
				} else if (currentScreen== 5 && !clicToShowStatistics ) {
                                    mainPanel.showMainView();
                          currentScreen = 1 ; 
					clicToShowStatistics = true;
					showStatistics.setText("Show Statistics >");
				}
			}
		});       
              
    }
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        showMatches = new javax.swing.JLabel();
        showGalerie = new javax.swing.JLabel();
        showStatistics = new javax.swing.JLabel();
        contactUs = new javax.swing.JLabel();

      
        

        showMatches.setText("Show Matches >");

        showGalerie.setText("Show Gallery >");

        showStatistics.setText("Show Statistics >");

        contactUs.setText("Contact Us >");
		
		jTable1.setModel(new MatchAffiche());
        jTable1.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
		jTable1.getColumnModel().getColumn(0).setPreferredWidth(28);
		jTable1.getColumnModel().getColumn(1).setPreferredWidth(78);
		jTable1.getColumnModel().getColumn(2).setPreferredWidth(25);
		jTable1.getColumnModel().getColumn(3).setPreferredWidth(25);
		jTable1.getColumnModel().getColumn(4).setPreferredWidth(25);
		jTable1.getColumnModel().getColumn(5).setPreferredWidth(25);
		jTable1.getColumnModel().getColumn(6).setPreferredWidth(25);
		jTable1.getColumnModel().getColumn(7).setPreferredWidth(25);
		jTable1.getColumnModel().getColumn(8).setPreferredWidth(25);
		jTable1.getColumnModel().getColumn(9).setPreferredWidth(25);
         jTable1.getTableHeader().setResizingAllowed(false);
		 jTable1.getTableHeader().setReorderingAllowed(false);
		jScrollPane1.setViewportView(jTable1);
		
		 validate(); 
		jScrollPane1.setViewportView(jTable1);
		
         javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 559, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 58, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(contactUs)
                    .addComponent(showStatistics)
                    .addComponent(showGalerie)
                    .addComponent(showMatches))
                .addGap(350, 350, 350))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(showMatches)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(showGalerie)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(showStatistics)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(contactUs)
                .addContainerGap(29, Short.MAX_VALUE))
        );
   
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        mainPanel.showAllMatchesPanel();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        mainPanel.showMainView();
        
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Classement.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Classement.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Classement.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Classement.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new Classement(mainPanel).setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel contactUs;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel showGalerie;
    private javax.swing.JLabel showMatches;
    private javax.swing.JLabel showStatistics;
    // End of variables declaration//GEN-END:variables

  
 
}
