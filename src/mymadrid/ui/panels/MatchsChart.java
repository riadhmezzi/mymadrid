package mymadrid.ui.panels;




import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;

import java.util.List;
import javax.swing.JPanel;

import mymadrid.soccer.Config;
import mymadrid.soccer.Group;
import mymadrid.soccer.Table.Row;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;

public class MatchsChart extends JPanel {

    private DefaultPieDataset dataset; //Objet qui va contenir les valeurs
    private JFreeChart graphe; // le graphe à dessiner
    private static List<Row> rows;

   

    public MatchsChart() {
        dataset = new DefaultPieDataset();
      
    }

    public void setValue(String title, Double numDouble) {
        dataset.setValue(title, numDouble);
    }

    public void setChart(String title) {
        graphe = ChartFactory.createPieChart(title, dataset, true, true, false);

        PiePlot pp = (PiePlot) graphe.getPlot();//l'Outil de dessin

        //pp.setSectionOutlinesVisible(false);
        pp.setLabelFont(new Font("SansSerif", Font.PLAIN, 12));
        pp.setNoDataMessage("Pas de données");
        pp.setCircular(true);
        pp.setLabelGap(0.005);
    }

    private JPanel createPanel() {
        return new ChartPanel(graphe);
    }

    public void Show() {
        add(createPanel());
        setVisible(true);
    }

    public void create() {
        setLayout(new BorderLayout());
        Config conf = new Config(1);
        Group grp = conf.getGroups().first();
        rows = grp.getTable().getRows();
       

        //*Connexion récupération des données
      
                for(Row r:rows)
                    if(r.getTeam().toString().equals("R. Madrid"))
                    {
                setValue("Won", (double)r.getMatchesWon());
                setValue("Lost", (double)r.getMatchesLost());
                setValue("Drawn", (double)r.getMatchesDrawn());
                    }
                //remplissage dataset
         
        /**/
        
        
        //setPreferredSize(getPreferredSize());
        //*Valeurs Statiques
//        chart.setValue("UN", new Double(20.0));
//        chart.setValue("DEUX", new Double(10.0));
//        chart.setValue("TROIS", new Double(20.0));
//        chart.setValue("QUATRE", new Double(30.0));
//        chart.setValue("CINQUE", new Double(20.0));
        /**/
       setChart("Matchs Stats");

        Show();
    }
    @Override
public Dimension getPreferredSize() {
    // given some values of w & h
    return new Dimension(250, 250);
}
}
