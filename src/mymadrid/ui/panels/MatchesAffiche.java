/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mymadrid.ui.panels;

import java.sql.SQLException;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;
import mymadrid.dao.MatchDao;
import mymadrid.entite.Match;
import mymadrid.soccer.Config;
import mymadrid.soccer.Group;
import mymadrid.soccer.Table.Row;

/**
 *
 * @author azzouze
 */
public class MatchesAffiche extends AbstractTableModel {

    MatchDao matchs = new MatchDao();
    Vector<Match> vectMatchs;
    private Vector<Match> rows;

    public MatchesAffiche() {
        Config conf = new Config(1);
        // System.out.println("empty ????" +conf.getGroups().isEmpty());
        Group grp = conf.getGroups().first();
       

        //   System.out.println("size"+ rows.size() );

        try {
            rows = matchs.getRealMatchs();
        } catch (SQLException ex) {
            Logger.getLogger(MatchAffiche.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public int getRowCount() {
        return rows.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

       

        switch (columnIndex) {
         



            case 0:

                return rows.get(rowIndex).getHomeTeam().toString();


            case 1:

                return rows.get(rowIndex).getHomeScore();

            case 2:
                 return rows.get(rowIndex).getGuestScore();

                

            case 3:

               return rows.get(rowIndex).getGuestTeam();

            

            default:

                return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        return null;
    }
}
