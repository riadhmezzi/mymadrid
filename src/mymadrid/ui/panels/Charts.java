/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mymadrid.ui.panels;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JPanel;

/**
 *
 * @author AZZOUZE
 */
public class Charts extends JPanel {
    private GoalsChart goalsChart;
        private GoalsForChart goalsForChart;
        private MatchsChart matchsChart;
    public Charts(){
        goalsChart=new GoalsChart();
        
        goalsForChart=new GoalsForChart();
        goalsForChart.create();
        goalsForChart.setVisible(true);
        matchsChart=new MatchsChart();
        matchsChart.create();
        matchsChart.setVisible(true);
        setLayout(new GridLayout(2, 1));
        JPanel p1=new JPanel();

        p1.setLayout(new GridLayout(1, 2));
        p1.add(goalsChart);
        p1.add(goalsForChart);
        JPanel p2=new JPanel();
        p2.setLayout(new FlowLayout());
        p1.setVisible(true);
        p2.setVisible(true);
        setVisible(true);

        p2.add(matchsChart);
        add(p1,0);
        add(p2,1);

    }




}
