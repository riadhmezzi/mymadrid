/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mymadrid.ui.panels;


import java.awt.Image;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import mymadrid.dao.PlayerDao;
import mymadrid.entite.Player;
import mymadrid.ui.MainPanel;

/**
 *
 * @author Riadh
 */
public class PlayersInfo extends javax.swing.JPanel {
public Random random = new Random();
    /**
     * Creates new form AllMatches
     */
    public PlayersInfo(final MainPanel mainPanel) throws SQLException, FileNotFoundException, IOException {
        initComponents();
        PlayerDao pdao = new PlayerDao();
         Vector<Player > playerVector = new Vector<Player >();
         playerVector = pdao.SelectPlaeyrs(); 
         //Random random = new Random(playerVector.size());
//         
//        for (Player p : pdao.SelectPlaeyrs()) {
//                        System.out.println(p.getImage()); 
//                        InputStream is = p.getImage() ; 
//                        //File file = new File
//                        FileOutputStream fos = new FileOutputStream(".\\imageFromDB\\tt.jpg"); 
//                        int b = 0;  
//                        while ((b = is.read()) != -1)  
//                        {  
//                            fos.write(b); // fos is the instance of FileOutputStream  
//                        }  
//            
//           ImageIcon icon = new ImageIcon(".\\imageFromDB\\tt.jpg"); 
//   
//        playerImage.setIcon(icon);  
//           
//                    }
        
        Timer timer = new Timer();
         timer.schedule( new TimerTask() {
             public void run() {
                 //playerImage.setIcon(null); 
                 PlayerDao pdao = new PlayerDao();
         Vector<Player > playerVector = new Vector<Player >();
                try {
                    playerVector = pdao.SelectPlaeyrs();
                } catch (SQLException ex) {
                    Logger.getLogger(PlayersInfo.class.getName()).log(Level.SEVERE, null, ex);
                }
                 
                int playernumber = random.nextInt(playerVector.size()); 
                
            //    System.out.println("vector size" + playerVector.size());
              //  System.out.println("random player" + random.nextInt(playerVector.size()));
                 
                Player p = playerVector.get(playernumber) ; 
                InputStream is = p.getImage() ; 
                
                FileOutputStream fos = null; 
                try {
                    fos = new FileOutputStream(".\\imageFromDB\\"+p.getNom()+".jpg");
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(PlayersInfo.class.getName()).log(Level.SEVERE, null, ex);
                }
                        int b = 0;  
                try {
                    while ((b = is.read()) != -1)  
                    {  
                        fos.write(b); // fos is the instance of FileOutputStream  
                    }
                } catch (IOException ex) {
                    Logger.getLogger(PlayersInfo.class.getName()).log(Level.SEVERE, null, ex);
                }
       
        ImageIcon icon = new ImageIcon(".\\imageFromDB\\"+p.getNom()+".jpg"); 

      
    Image img = icon.getImage();
    
    
   ///to keep ratio 
    int iconWidth = icon.getIconWidth();
  int iconHeight = icon.getIconHeight();
  double iconAspect = (double) iconHeight / iconWidth;

  int w = 200;
  int h = 175;
  double canvasAspect = (double) h / w;

  int x = 0, y = 0;
  
  // Maintain aspect ratio.
  if(iconAspect < canvasAspect)
  {
   // Drawing space is taller than image.
   y = h;
   h = (int) (w * iconAspect);
   y = (y - h) / 2; // center it along vertical
  }
  else
  {
   // Drawing space is wider than image.
   x = w;
   w = (int) (h / iconAspect);
   x = (x - w) / 2; // center it along horizontal
  }
  //end to keep ratio
        
    Image imgTemp = img.getScaledInstance(w + x , h+ y , 1);
    
    icon.setImage(imgTemp); 
    
    
        playerImage.setIcon(icon); 
        playerNumber.setText(String.valueOf(p.getNumber()));
        playerName.setText(p.getNom().toUpperCase());
        playerFullName.setText(p.getNom() +" "+ p.getPrenom());
        playerCitation.setText("<html>"+ p.getCitation() + "</html>");
        
        playerWeight.setText(p.getPoids());
        playerHeight.setText(p.getLongeur());
        playerPosition.setText(p.getPosition());
        playerNationality.setText(p.getNationalite());
        playerDate.setText(String.valueOf(p.getDate_naiss()));
        playerPlace.setText(p.getVille_naiss());
        
        
               
                  }
             }, 0, 5*1000);
     
       
       // System.out.println("my size"+ playerImage.getWidth() +" hh" + playerImage.getHeight());
        

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        playerImage = new javax.swing.JLabel();
        playerNumber = new javax.swing.JLabel();
        playerName = new javax.swing.JLabel();
        playerFullName = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        playerCitation = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        playerPlace = new javax.swing.JLabel();
        playerDate = new javax.swing.JLabel();
        playerPosition = new javax.swing.JLabel();
        playerWeight = new javax.swing.JLabel();
        playerHeight = new javax.swing.JLabel();
        playerNationality = new javax.swing.JLabel();

        playerNumber.setFont(new java.awt.Font("Calibri", 1, 70)); // NOI18N
        playerNumber.setForeground(new java.awt.Color(82, 95, 114));
        playerNumber.setText("1");

        playerName.setFont(new java.awt.Font("Calibri", 1, 36)); // NOI18N
        playerName.setForeground(new java.awt.Color(41, 47, 57));
        playerName.setText("CASILLAS");

        playerFullName.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        playerFullName.setForeground(new java.awt.Color(72, 202, 238));
        playerFullName.setText("Iker Casillas Fernandez");

        playerCitation.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        playerCitation.setForeground(new java.awt.Color(219, 181, 43));
        playerCitation.setText("I am a winner whose work is based on humiliy");
        playerCitation.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("Place of Birth:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setText("Date of Birth: ");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("Position:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Weight: ");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("Height:");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel6.setText("Nationality:");

        playerPlace.setText("spain");

        playerDate.setText("jLabel7");

        playerPosition.setText("jLabel7");

        playerWeight.setText("jLabel7");

        playerHeight.setText("jLabel7");

        playerNationality.setText("jLabel7");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(playerImage, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(playerNumber)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(playerName)
                            .addComponent(playerFullName)
                            .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(playerCitation, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 92, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(playerDate))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel6)
                            .addComponent(jLabel5)
                            .addComponent(jLabel4)
                            .addComponent(jLabel3))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(playerPosition)
                            .addComponent(playerWeight)
                            .addComponent(playerHeight)
                            .addComponent(playerNationality)
                            .addComponent(playerPlace))))
                .addGap(31, 31, 31))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(playerImage, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(57, 57, 57)
                        .addComponent(playerFullName, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(playerCitation, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(playerNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel1)
                                    .addComponent(playerPlace))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel2)
                                    .addComponent(playerDate))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel3)
                                    .addComponent(playerPosition))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel4)
                                    .addComponent(playerWeight))
                                .addGap(10, 10, 10)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel5)
                                    .addComponent(playerHeight))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel6)
                                    .addComponent(playerNationality)))
                            .addComponent(playerName, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(103, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
  
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel playerCitation;
    private javax.swing.JLabel playerDate;
    private javax.swing.JLabel playerFullName;
    private javax.swing.JLabel playerHeight;
    private javax.swing.JLabel playerImage;
    private javax.swing.JLabel playerName;
    private javax.swing.JLabel playerNationality;
    private javax.swing.JLabel playerNumber;
    private javax.swing.JLabel playerPlace;
    private javax.swing.JLabel playerPosition;
    private javax.swing.JLabel playerWeight;
    // End of variables declaration//GEN-END:variables
}
