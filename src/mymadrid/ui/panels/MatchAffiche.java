/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mymadrid.ui.panels;

import java.sql.SQLException;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;
import mymadrid.dao.MatchDao;
import mymadrid.entite.Match;
import mymadrid.soccer.Config;
import mymadrid.soccer.Group;
import mymadrid.soccer.Table.Row;

/**
 *
 * @author Riadh
 */
public class MatchAffiche extends AbstractTableModel {

    MatchDao matchs = new MatchDao();
    String[] columns = {"Pos", "Equipe", "MJ", "V", "N", "P", "BP", "BC", "Dif", "Pt"};
    Vector<Match> vectMatchs;
    private List<Row> rows;

    public MatchAffiche() {
        Config conf = new Config(1);
    
        Group grp = conf.getGroups().first();
        rows = grp.getTable().getRows();


        try {
            vectMatchs = matchs.SelectMatch();
        } catch (SQLException ex) {
            Logger.getLogger(MatchAffiche.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public int getRowCount() {
        return rows.size();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        

        switch (columnIndex) {
            case 0:

                return rowIndex + 1;



            case 1:

                return rows.get(rowIndex).getTeam().getId();


            case 2:

                return rows.get(rowIndex).getMatchesPlayed();

            case 3:

                return rows.get(rowIndex).getMatchesWon();

            case 4:

                return rows.get(rowIndex).getMatchesDrawn();

            case 5:

                return rows.get(rowIndex).getMatchesLost();

            case 6:

                return rows.get(rowIndex).getGoalsFor();
                  case 7:

                return rows.get(rowIndex).getGoalsAgainst();
                        case 8:

               return rows.get(rowIndex).getGoalsDifference();
                        case 9:
                            return rows.get(rowIndex).getPoints();

            default:

                return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        return columns[column];
    }
}
