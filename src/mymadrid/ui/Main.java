package mymadrid.ui;


import aurelienribon.ui.components.ArStyle;
import aurelienribon.ui.css.swing.SwingStyle;
import java.io.File;
import java.io.IOException;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import org.apache.commons.io.FileUtils;

/**
 * @author Riadh
 */
public class Main {
	public static void main(String[] args) {
		parseArgs(args);

		SwingUtilities.invokeLater(new Runnable() {
			@Override public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (ClassNotFoundException ex) {
				} catch (InstantiationException ex) {
				} catch (IllegalAccessException ex) {
				} catch (UnsupportedLookAndFeelException ex) {
				}

				SwingStyle.init();
				ArStyle.init();

				JFrame frame = new JFrame("funKoora");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setContentPane(new MainPanel());
				frame.setSize(1100, 600);
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
		});
	}

	private static void parseArgs(String[] args) {
		for (int i=0; i<args.length; i++) {
			if (args[i].equals("-testliburl") && i<args.length) {
				

			} else if (args[i].equals("-testlibdef") && i<args.length) {
				File file = new File(args[i+1]);
			}
		}
	}
}
