package mymadrid.soccer;

import static mymadrid.soccer.ranking.Ranking.*;

import java.util.GregorianCalendar;
import java.util.SortedSet;
import java.util.TreeSet;

public class Config
{
	private SortedSet<Group> groups = new TreeSet<Group>();

    public SortedSet<Group> getGroups() {
        return groups;
    }

	// Real match tables of Primera Divisi�n (Spain), downloaded from http://fussballdaten.de
	private static final boolean PRIMERA_DIVISION_GROUPS = true;
	

	// Print options
	private static final boolean SHOW_TEAMS = false;
	private static final boolean SHOW_MATCHES = false;
	private static final boolean SHOW_SUBTABLES = false;

        public Config(){
            
        }
        public Config(int a){
                
            //Config config = new Config();
		this.initialiseGroups();
		this.printGroups();
        }
        
        
	public static void main(String[] args) {
         
		Config config = new Config();
		config.initialiseGroups();
		config.printGroups();
	}

	private void initialiseGroups() {
		groups.clear();
		if (PRIMERA_DIVISION_GROUPS) {

			initialise_Primera_Division_2011_12();
		}
		
	}

	private void printGroups() {
		for (Group group : groups)
			group.print(System.out, SHOW_TEAMS, SHOW_MATCHES, SHOW_SUBTABLES);
	}





	private void initialise_Primera_Division_2011_12() {
		/*
		 * - Getafe (GD -11) is ranked higher than Sociedad (GD -6) and Betis (GD -9) even though the
		 *   overall GD is the worst of the three teams, because in direct comparison it is 8 points Getafe,
		 *   5 points Sociedad, 2 points Betis.
		 */
		String rawData =
			"FC Barcelona  ,---,1,3,0,0,1,1,4,1,4,2,7,1,4,1,3,0,4,1,5,1,4,2,4,1,5,0,6,2,3,1,5,1,3,0,3,1,4,0\n" +
			"R. Madrid ,1,2,---,5,1,4,1,5,0,5,0,8,0,5,0,0,0,2,0,2,1,4,0,4,2,4,0,4,0,4,0,5,3,5,0,3,1,3,0\n" +
			"A. Madrid    ,2,3,2,2,---,2,0,1,0,1,1,4,0,2,2,1,2,1,1,0,1,3,1,4,0,2,1,4,1,1,2,1,0,1,0,4,0,4,3\n" +
			"Malaga      ,0,4,1,4,1,0,---,0,0,1,0,1,1,3,1,2,1,1,0,1,1,3,2,0,2,2,1,4,2,5,1,4,0,2,1,1,0,3,0\n" +
			"Valencia   ,1,4,1,2,0,0,2,1,---,3,2,0,0,1,1,0,0,2,1,1,1,3,0,0,2,3,1,3,1,3,1,2,0,3,0,4,0,4,0\n" +
			"Levante     ,1,0,1,2,0,2,3,0,2,0,---,0,2,0,0,1,0,3,0,3,2,1,2,3,1,3,1,3,5,0,0,3,1,1,0,4,0,1,1\n" +
			"Osasuna     ,1,5,3,2,1,1,1,1,0,1,2,0,---,2,2,0,0,2,1,1,0,0,0,2,1,2,0,0,0,3,0,2,1,2,1,2,1,0,2\n" +
			"Mallorca    ,1,2,0,2,1,1,0,1,2,1,1,0,1,1,---,0,0,1,1,2,1,1,2,1,0,1,0,1,0,1,0,0,0,4,0,1,2,2,1\n" +
			"Sevilla     ,2,6,0,2,1,0,2,1,1,1,1,1,2,0,3,1,---,1,2,1,0,3,0,1,2,0,0,5,2,3,0,1,2,1,2,2,1,2,2\n" +
			"Bilbao      ,0,3,2,2,0,3,3,0,3,0,3,0,3,1,1,0,1,0,---,2,0,0,0,2,3,3,3,1,1,2,1,0,1,1,1,1,1,1,1\n" +
			"Sociedad    ,0,1,2,2,1,0,3,2,0,4,1,3,0,0,1,0,2,0,1,2,---,0,0,1,1,0,0,4,0,3,0,1,0,1,1,5,1,3,0\n" +
			"Getafe      ,0,1,1,0,3,1,1,3,3,2,1,1,2,2,1,3,5,1,0,0,1,0,---,1,0,1,1,0,1,0,2,1,0,0,0,2,0,1,1\n" +
			"Betis       ,2,3,2,2,2,1,0,0,2,2,0,1,1,0,1,0,1,1,2,1,2,3,1,1,---,1,1,0,2,4,3,1,2,3,1,2,0,1,1\n" +
			"Es Barcelona,0,4,1,1,4,0,1,2,4,2,1,2,1,2,1,0,1,1,2,1,2,2,1,0,1,0,---,5,1,0,2,3,0,0,0,0,3,3,1\n" +
			"Vallecano   ,0,1,0,7,1,2,2,0,0,1,1,2,6,0,0,1,2,1,2,3,4,0,2,0,3,0,0,1,---,0,0,1,0,0,2,1,3,4,2\n" +
			"Saragossa   ,0,6,1,4,0,1,0,0,1,0,1,0,1,1,0,1,0,1,2,0,2,0,1,1,0,2,2,1,1,2,---,1,0,2,1,2,2,2,1\n" +
			"Granada     ,1,2,0,1,0,1,2,1,0,0,2,1,1,1,2,2,0,3,2,2,4,1,1,0,0,1,2,1,1,2,1,0,---,1,0,2,1,0,0\n" +
			"Villarreal  ,1,1,0,0,2,2,2,1,0,1,0,3,1,1,2,0,2,2,2,2,1,1,1,2,1,0,0,0,2,0,2,2,3,1,---,3,0,1,1\n" +
			"Gijon       ,0,3,0,1,0,1,2,1,1,1,3,2,1,1,2,3,1,0,1,1,1,2,2,1,2,1,1,2,2,1,1,2,2,0,2,3,---,0,0\n" +
			"Santander   ,0,0,0,2,2,2,1,3,0,0,0,0,2,4,0,3,0,3,0,1,0,0,1,2,1,0,0,1,1,1,1,0,0,1,1,0,1,1,---\n";
                
		groups.add(Group.parseCrossTable("PD_2011_12", "Primera Division 2011/12", rawData, PRIMERA_DIVISION.comparator));
	}

	

	

	

}