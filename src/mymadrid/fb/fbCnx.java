/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mymadrid.fb;

import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserAdapter;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserNavigationEvent;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.types.Page;
import com.restfb.types.User;
import java.awt.BorderLayout;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Date;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;
import mymadrid.dao.UserDao;
import mymadrid.entite.UserApp;
import mymadrid.mail.MailFrame;
import mymadrid.ui.MainPanel;
import mymadrid.ui.panels.Classement;
import mymadrid.ui.panels.Welcom;

/**
 *
 * @author Riadh
 */
public class fbCnx {
    
    
    public static UserApp currentUser ; 
    
    // les attribus 
    public static String API_KEY = "112568895608877";
    public static String SECRET = "0f388ed62addc9684a5332fe05b6b709";
    public static String firstRequest = "https://graph.facebook.com/oauth/authorize?"
            + "client_id="
            + API_KEY
            + "&redirect_uri=http://www.facebook.com/connect/login_success.html&"
            + "scope=publish_stream,offline_access,create_event,read_stream,email,user_birthday";
    public static String secondRequest = "https://graph.facebook.com/oauth/access_token?"
            + "client_id="
            + API_KEY
            + "&redirect_uri=http://www.facebook.com/connect/login_success.html&"
            + "client_secret=" + SECRET + "&code=";
    public static String access_token = "";
    public static boolean firstRequestDone = false;
    public static boolean secondRequestDone = false;
    final JFrame loginFrame = new JFrame();

    public static FacebookClient facebookClient = null;
    
    private String userName ; 
    private String userMail ; 
    
    public static boolean fbCnxEnd = false; 
    
    MainPanel context ; 
    public fbCnx(MainPanel context) {
        this.context = context ; 
    }
    
    public  void loginFb(){
        NativeInterface.open();
        NativeInterface.initialize();
        final JFrame authFrame = new JFrame();
        JPanel webBrowserPanel = new JPanel(new BorderLayout());
        final JWebBrowser webBrowser = new JWebBrowser();
        webBrowser.navigate(firstRequest);
        webBrowser.addWebBrowserListener(new WebBrowserAdapter() {

            @Override
            public void locationChanged(WebBrowserNavigationEvent e) {
                super.locationChanged(e);
                // Check if first request was not done
                if (!firstRequestDone) {
                    // Check if you left the location and were redirected to the next
                    // location
                    if (e.getNewResourceLocation().contains("http://www.facebook.com/connect/login_success.html?code=")) {
                        // If it successfully redirects you, get the verification code
                        // and go for a second request
                        String[] splits = e.getNewResourceLocation().split("=");
                        String stage2temp = secondRequest + splits[1];
                        System.out.println("First =" + splits[1]);
                        webBrowser.navigate(stage2temp);
                        firstRequestDone = true;
                    }
                } else {
                    // If secondRequest is not done yet, you perform this and get the
                    // access_token
                    if (!secondRequestDone) {
                        System.out.println(webBrowser.getHTMLContent());
                        // Create reader with the html content
                        StringReader readerSTR = new StringReader(webBrowser.getHTMLContent());
                        // Create a callback for html parser
                        HTMLEditorKit.ParserCallback callback =
                                new HTMLEditorKit.ParserCallback() {

                                    @Override
                                    public void handleText(char[] data, int pos) {
                                        try {
                                            System.out.println(data);
                                            // because there is only one line with the access_token
                                            // in the html content you can parse it.
                                            String string = new String(data);
                                            String[] temp1 = string.split("&");
                                            String[] temp2 = temp1[0].split("=");
                                            System.out.println("access tocken=" + temp2);
                                            access_token = temp2[1];
                                            new GraphReaderExample(access_token).runEverything();
                                            facebookClient = new DefaultFacebookClient(access_token);
                                            User user = facebookClient.fetchObject("me", User.class);
                                            Page page = facebookClient.fetchObject("cocacola", Page.class);
                                            // userName = user.getName();
                                           //  userMail = user.getEmail();
                                            
                                            authFrame.dispose();
                                            /********/
                                            String userName = user.getName();
                                            String userMail = user.getEmail();
                                            UserDao udao = new UserDao();
                                            currentUser = new UserApp(userName, userMail);
                                            UserApp u = currentUser ; 
                                          
                                            if (u.equals(udao.findByEmail(userMail))) {
                                                  
                                               JOptionPane.showMessageDialog(context, "We have you in our database! So welcome :)",
                                                        "Welcome", JOptionPane.INFORMATION_MESSAGE);
                                              //  fbCnx.this.setVisible(false);
                                              
                                            }else
                                            {
                                             udao.InsertUser(u);
                                            }
                                        } catch (SQLException ex) {
                                            Logger.getLogger(fbCnx.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    }
                                };
                        try {
                            // Call the parse method
                            new ParserDelegator().parse(readerSTR, callback, false);

                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                        // After everything is done, you can dispose the jframe
                        authFrame.dispose();
                        fbCnxEnd = true ; 
                        System.out.println("endddd");
                        context.showMainViewFistTime();
                   
        Welcom.welcom1.setText("<html> Welcome "+  "<FONT COLOR=#DBA61E > "+currentUser.getUserName()+"</Font> "  +" To Real Madrid Application version 1 developed by Riadh Mezzi and AbdelAziz Halaouet at <FONT COLOR=#C11B17 >ESPRIT 2013</Font> <br> <br> <br> Real Madrid Club de Fútbol, commonly known as Real Madrid, is a professional football club based in Madrid, Spain. Founded in 1902 as Madrid Football Club, has traditionally worn a white home kit since  </html>");
        MailFrame.sender.setText((currentUser.getFbEmail()));               
                        
                    }
                }
            }
        });

        webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
        authFrame.add(webBrowserPanel);
        authFrame.setSize(900, 700);
        authFrame.setVisible(true);
        authFrame.setLocationRelativeTo(null);
      
    }
    
    
}
