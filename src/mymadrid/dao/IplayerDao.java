/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mymadrid.dao;

import java.sql.SQLException;
import java.util.Vector;
import mymadrid.entite.Player;


/**
 *
 * @author Riadh
 */
public interface IplayerDao {


    public int InsertPlayer(Player p) throws SQLException;

    public boolean DeletePlayer(Player p) throws SQLException;

    public Vector<Player> SelectPlayer() throws SQLException;

    public boolean UpdatePlayer(Player p) throws SQLException;

    public Player   findId(String id) throws SQLException;
}
