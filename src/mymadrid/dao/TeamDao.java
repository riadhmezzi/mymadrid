package mymadrid.dao;


import java.sql.*;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import mymadrid.technique.ConnexionBD;
import mymadrid.entite.Team;


/**
 *
 * @author riadh
 */
public class TeamDao implements ITeamDao {

    Connection connect;
    Statement ste;
    PreparedStatement step;
    ResultSet res;

    public TeamDao() {
        try {
            connect = ConnexionBD.getInstance();
            ste = connect.createStatement();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int InsertTeam(Team m) throws SQLException {
        int key = 0;
        String QueryInsert = "insert into `team` (`id`,`name`) values('" +m.getId() + "' , '" + m.getName()+ "' ); ";
        ste.executeUpdate(QueryInsert, ste.RETURN_GENERATED_KEYS);
        res = ste.getGeneratedKeys();
        while (res.next()) {
            key = res.getInt(1);
        }

        return key;
    }

    public boolean UpdateTeam(Team m) throws SQLException {
        String QueryUpdate = "Update match SET id = ? , name = ? where id =  ? ;";
        step = connect.prepareStatement(QueryUpdate);
       // step.setInt(4, m.getId());
        step.setString(1, m.getId());
        step.setString(2, m.getName());
        step.setString(3, m.getName());
       

        step.executeUpdate();

        return true;
    }

    public boolean DeleteTeam(Team m) throws SQLException {
        boolean test = false;
        String QueryDelete = "Delete from match where id = ? ;";
        step = connect.prepareStatement(QueryDelete);
        step.setString(1, m.getId());
        step.executeUpdate();
        return true;
    }

    public Vector<Team> SelectTeam() throws SQLException {

        Vector<Team > teamVector = new Vector<Team >();
        String QuerySelect = "Select * from `team`";
        res = ste.executeQuery(QuerySelect);

        while (res.next()) {
            Team m = new Team();
            m.setId(res.getString(1));
            m.setName(res.getString(2));
           
            teamVector.add(m);
        }
        return teamVector;
    }


   
    @Override
    public Team findId(String id) throws SQLException {
        Team m = null;


        String QuerySelect = "Select * from team  where id = ? ;";

        step = connect.prepareStatement(QuerySelect);

        res = step.executeQuery();

        while (res.next()) {
            m = new Team();
            m.setId(res.getString("id"));
            m.setName(res.getString("name"));
            

        }

        return m;
    }

}
