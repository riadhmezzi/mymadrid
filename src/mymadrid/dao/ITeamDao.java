/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mymadrid.dao;

import java.sql.SQLException;
import java.util.Vector;
import mymadrid.entite.Team;

/**
 *
 * @author Riadh
 */
public interface ITeamDao {


    public int InsertTeam(Team m) throws SQLException;

    public boolean DeleteTeam(Team m) throws SQLException;

    public Vector<Team> SelectTeam() throws SQLException;

    public boolean UpdateTeam(Team m) throws SQLException;

    public Team  findId(String id) throws SQLException;
}
