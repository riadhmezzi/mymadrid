package mymadrid.dao;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.*;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import mymadrid.entite.Player;
import mymadrid.technique.ConnexionBD;
import mymadrid.entite.Team;


/**
 *
 * @author riadh
 */
public class PlayerDao implements IplayerDao {

    Connection connect;
    Statement ste;
    PreparedStatement step;
    ResultSet res;

    public PlayerDao() {
        try {
            connect = ConnexionBD.getInstance();
            ste = connect.createStatement();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int InsertPlayer(Player p) throws SQLException {
        
        PreparedStatement st = connect.prepareStatement("INSERT INTO `players` ( `nom`, `prenom`, `date_naiss`, `ville_naiss`, `Position`, `poids`, `longeur`, `nationalite`, `citation`, `number`, `image`) VALUES (?,?,?,?,?,?,?,?,?,?,?) ; ");
        st.setString(1, p.getNom());
        st.setString(2, p.getPrenom());
        st.setDate(3, p.getDate_naiss());
        st.setString(4, p.getVille_naiss());
        st.setString(5, p.getPosition());
        st.setString(6, p.getPoids());
        st.setString(7, p.getLongeur());
        st.setString(8, p.getNationalite());
        st.setString(9, p.getCitation());
        st.setInt(10, p.getNumber());
        
     
        
        
        
     File fBlob = new File ( p.getImagePath() );
    FileInputStream is = null;
        try {
            is = new FileInputStream ( fBlob );
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PlayerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
      st.setBinaryStream (11, is, (int) fBlob.length() );
                    
       // System.out.println(st.toString()); 
        st.execute();
        //ste.executeUpdate(QueryInsert, ste.RETURN_GENERATED_KEYS);
        

        return 1;
    }

    public boolean UpdatePlyaer(Player p) throws SQLException {
        String QueryUpdate = "Update player SET id = ? , name = ? where id =  ? ;";
        step = connect.prepareStatement(QueryUpdate);
       // step.setInt(4, m.getId());
//        step.setString(1, m.getId());
//        step.setString(2, m.getName());
//        step.setString(3, m.getName());
       

        step.executeUpdate();

        return true;
    }

    @Override
    public boolean DeletePlayer(Player p) throws SQLException {
        boolean test = false;
        String QueryDelete = "Delete from player where id = ? ;";
        step = connect.prepareStatement(QueryDelete);
        step.setInt(1, p.getId());
        step.executeUpdate();
        return true;
    }

    public Vector<Player> SelectPlaeyrs() throws SQLException {

        Vector<Player > playerVector = new Vector<Player >();
        String QuerySelect = "Select * from `players`";
        res = ste.executeQuery(QuerySelect);

        while (res.next()) {
            Player p = new Player();
            p.setId(res.getInt(1));
            p.setNom(res.getString(2));
            p.setPrenom(res.getString(3));
            p.setDate_naiss(res.getDate(4));
            p.setVille_naiss(res.getString(5));
            p.setPosition(res.getString(6));
            p.setPoids(res.getString(7));
            p.setLongeur(res.getString(8));
            p.setNationalite(res.getString(9));
            p.setCitation(res.getString(10));
            p.setNumber(res.getInt(11));
            p.setImage(res.getBinaryStream(12));     
       
            
            
            
            playerVector.add(p);
        }
        return playerVector;
    }


   
    @Override
    public Player findId(String id) throws SQLException {
        Player p = null;


        String QuerySelect = "Select * from team  where id = ? ;";

        step = connect.prepareStatement(QuerySelect);

        res = step.executeQuery();
//
//        while (res.next()) {
//            p = new Player();
//            p.setId(res.getString("id"));
//            p.setName(res.getString("name"));
//            
//
//        }

        return p;
    }



    @Override
    public Vector<Player> SelectPlayer() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean UpdatePlayer(Player p) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

  

 

}
