package mymadrid.dao;


import java.sql.*;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import mymadrid.technique.ConnexionBD;
import mymadrid.entite.Match; 


/**
 *
 * @author Riadh
 */
public class MatchDao implements ImatchDao {

    Connection connect;
    Statement ste;
    PreparedStatement step;
    ResultSet res;
    
    public MatchDao() {
        try {
            connect = ConnexionBD.getInstance();
            ste = connect.createStatement();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int InsertMatch(Match m) throws SQLException {
        int key = 0;
        String QueryInsert = "insert into `match` (`equipe_domicile`,`equipe_invite`,`score_domicile`,`score_invite` ) values('" +m.getHomeTeam() + "' , '" + m.getGuestTeam()+ "' , '" +m.getHomeScore()+"','"+m.getGuestScore()+"'); ";
        ste.executeUpdate(QueryInsert, ste.RETURN_GENERATED_KEYS);
        res = ste.getGeneratedKeys();
        while (res.next()) {
            key = res.getInt(1);
        }

        return key;
    }

    public boolean UpdateMatch(Match m) throws SQLException {
        String QueryUpdate = "Update match SET equipe_domicile = ? , equipe_invite = ?,score_domicile = ?,score_invite= ?  , where id =  ? ;";
        step = connect.prepareStatement(QueryUpdate);
       // step.setInt(4, m.getId());
        step.setString(1, m.getHomeTeam().toString());
        step.setString(2, m.getGuestTeam().toString());
        step.setInt(3, m.getHomeScore());
        step.setInt(4, m.getGuestScore());
       
        step.executeUpdate();

        return true;
    }

    @Override
    public boolean DeleteMatch(Match m) throws SQLException {
        boolean test = false;
        String QueryDelete = "Delete from match where id = ? ;";
        step = connect.prepareStatement(QueryDelete);
        step.setInt(1, m.getId());
        step.executeUpdate();
        return true;
    }

    
    public Vector<Match> SelectMatch() throws SQLException {

        Vector<Match > matchVector = new Vector<Match >();
        String QuerySelect = "Select * from `match`";
        res = ste.executeQuery(QuerySelect);

        while (res.next()) {
            Match m = new Match(); 
            m.setId(res.getInt(1));
            m.setDate(res.getDate(2));
            m.setHomeTeam(res.getString(3));
            m.setGuestTeam(res.getString(4));
            m.setHomeScore(res.getInt(5));
            m.setGuestScore(res.getInt(6));
            matchVector.add(m);
        }
        return matchVector;
    }




    @Override
    public Match findId(int id) throws SQLException {
        Match m = null;


        String QuerySelect = "Select * from users  where id = ? ;";

        step = connect.prepareStatement(QuerySelect);
      
        res = step.executeQuery();

        while (res.next()) {
            m = new Match();
            m.setId(res.getInt("id"));
            m.setDate(res.getDate("date"));
            m.setHomeTeam(res.getString("equipe_domicile"));
            m.setGuestTeam(res.getString("equipe_invite"));
            m.setHomeScore(res.getInt("score_domicile"));
            m.setHomeScore(res.getInt("score_invite"));
        
        }

        return m;
    }
    @Override
    public Vector<Match> getRealMatchs() throws SQLException {

        Vector<Match > matchVector = new Vector<Match >();
        String QuerySelect = "Select * from `match` where equipe_domicile='R. Madrid' or equipe_invite='R. Madrid' order by date";
        res = ste.executeQuery(QuerySelect);


        while (res.next()) {
            Match m = new Match();
            m.setId(res.getInt(1));
            m.setDate(res.getDate(2));
            m.setHomeTeam(res.getString(3));
            m.setGuestTeam(res.getString(4));
            m.setHomeScore(res.getInt(5));
            m.setGuestScore(res.getInt(6));
            matchVector.add(m);
        }
        return matchVector;
    }


}
