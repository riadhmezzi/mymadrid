/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mymadrid.dao;

import mymadrid.entite.UserApp; 
import java.sql.SQLException;
import java.util.Vector;

/**
 *
 * @author Riadh
 */
public interface IUserDao {


    public int InsertUser(UserApp u) throws SQLException;

    public boolean DeleteUser(UserApp u) throws SQLException;

    public Vector<UserApp> SelectUser() throws SQLException;

    public boolean UpdateUser(UserApp u) throws SQLException;

    public UserApp findByEmail(String fbMail) throws SQLException;
}
