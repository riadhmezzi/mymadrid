package mymadrid.dao;


import java.sql.*;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import mymadrid.technique.ConnexionBD;
import mymadrid.entite.UserApp; 


/**
 *
 * @author riadh
 */
public class UserDao implements IUserDao {

    Connection connect;
    Statement ste;
    PreparedStatement step;
    ResultSet res;
    
    public UserDao() {
        try {
            connect = ConnexionBD.getInstance();
            ste = connect.createStatement();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int InsertUser(UserApp u) throws SQLException {
        int key = 0;
        String QueryInsert = "insert into users values( '" + u.getUserId() + "' , '" + u.getUserName() + "' , '" + u.getFbEmail() +"'); ";
        ste.executeUpdate(QueryInsert, ste.RETURN_GENERATED_KEYS);
        res = ste.getGeneratedKeys();
        while (res.next()) {
            key = res.getInt(1);
        }

        return key;
    }

    public boolean UpdateUser(UserApp u) throws SQLException {
        String QueryUpdate = "Update users SET userName = ? , fbEmail = ? , where userId =  ? ;";
        step = connect.prepareStatement(QueryUpdate);
        step.setInt(4, u.getUserId());
        step.setString(1, u.getUserName());
        step.setString(2, u.getFbEmail());
       
        step.executeUpdate();

        return true;
    }

    public boolean DeleteUser(UserApp u) throws SQLException {
        boolean test = false;
        String QueryDelete = "Delete from users where userId = ? ;";
        step = connect.prepareStatement(QueryDelete);
        step.setInt(1, u.getUserId());
        step.executeUpdate();
        return true;
    }

    public Vector<UserApp> SelectUser() throws SQLException {

        Vector<UserApp> userVector = new Vector<UserApp>();
        String QuerySelect = "Select * from users;";
        res = ste.executeQuery(QuerySelect);

        while (res.next()) {
            UserApp e = new UserApp();
            e.setUserId(res.getInt("userId"));
            e.setUserName(res.getString("userName"));
            e.setFbEmail(res.getString("fbEmail"));
            userVector.add(e);
        }
        return userVector;
    }

    public UserApp findByEmail(String fbMail) throws SQLException {
        UserApp u = null;


        String QuerySelect = "Select * from users  where fbEmail = ? ;";

        step = connect.prepareStatement(QuerySelect);
        step.setString(1, fbMail);
        res = step.executeQuery();

        while (res.next()) {
            u = new UserApp();
            u.setUserId(res.getInt(1));
            u.setFbEmail(res.getString(3));
            u.setUserName(res.getString(2));
        
        }

        return u;
    }

}
