/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mymadrid.dao;

import mymadrid.entite.Match; 
import java.sql.SQLException;
import java.util.Vector;

/**
 *
 * @author Riadh
 */
public interface ImatchDao {


    public int InsertMatch(Match m) throws SQLException;

    public boolean DeleteMatch(Match m) throws SQLException;

    public Vector<Match> SelectMatch() throws SQLException;

    public boolean UpdateMatch(Match m) throws SQLException;

    public Match  findId(int id) throws SQLException;
     public Vector<Match> getRealMatchs() throws SQLException;

}
